# Types & Data Structures - HW 2
# Авустин Пенья

#Встроенная функция input позволяет ожидать и возвращать данные из стандартного
#ввода в виде строки (весь введенный пользователем текст до нажатия им enter).
#Используя данную функцию, напишите программу, которая:

#1. После запуска предлагает пользователю ввести текст.
#2. В качестве ответа печатает наиболее часто встречающееся в тексте слово
#или несколько таких слов, если имеет место "ничья". Также указывая
#сколько именно раз слово встретилось в тексте. (Игнорируйте заглавные буквы
#при отождествлении слов - то есть считайте слова "Подлодка" и "подлодка"
#одинаковыми, а разные формы слов - разными словами)
#После чего ждет следующего ввода.
#3.При получении в качестве вводных данных 'cancel' завершает свою работу.

# create while loop
while True:

# input string, create empty dict and split string into a list of words
    string = input('Enter a few words: ')
    dict = {}
    words = string.split()

# set condition for breaking the loop
    if string  == 'cancel':
        print('Bye!')
        break

# turn all words into lower case
    for i in range(len(words)):
        words[i] = words[i].lower()

# count occurences for each different word
    for x in set(words):
        dict[x] = words.count(x)

# find out max number of ocurrences
    m = max(dict.values())

# print words with maximum num. of occurences
    for y in dict.keys():
        if dict[y] == m:
            print(str(m) + ' - ' + str(y))
