# Functions_2 - hw3
# Августин Пенья

def count_points(a, b, c):
     import math
     x1, y1 = a
     x2, y2 = b
     x3, y3 = c
     points_side1 = math.gcd(abs(x1-x2), abs(y1-y2))
     points_side2 = math.gcd(abs(x1-x3), abs(y1-y3))
     points_side3 = math.gcd(abs(x2- x3), abs(y2 - y3))
     area = abs(x1*(y2-y3) + x2*(y3-y1) + x3*(y1-y2)) * 1/2
     b = points_side1 + points_side2 + points_side3
     i = 1 + area - b*1/2
     return i + b
