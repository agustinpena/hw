
def letters_range(*args):
	alphab = {chr(i+97):i for i in range(26)}
	letters = []
	args = [str(x) for x in args]
	largs = [alphab[x] if x.isalpha() else int(x) for x in args]
	for y in range(*largs):
		letters.append(list(alphab.keys())[y])
	return letters

