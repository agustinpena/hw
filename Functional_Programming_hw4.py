# Functional_Programming, HW 4
# Project Euler, exercises.

# Exercise 9
# For a Pythagorean triplet (a,b,c) such that a<b<c and a+b+c=1000, find abc.

print([(1000-b-c)*b*c for c in range(1000) for b in range(c) if c**2==b**2+(1000-b-c)**2 and 0<1000-b-c<b])

# Exercise 6
#Find the difference between the sum of the squares of the first
# one hundred natural numbers and the square of the sum.
import functools as ft
print(ft.reduce((lambda a,b:a+b), [i for i in range(101)])**2 - ft.reduce((lambda a,b:a+b), [i*i for i in range(101)]))

# Exercise 48
#Find the last ten digits of the series, 1**1 + 2**2 + 3**3 + ... + 1000**1000.
import functools as ft
print(str(ft.reduce((lambda a,b:a+b), [i**i for i in range(1001)]))[-10:])

# Exercise 40
#An irrational decimal fraction is created by concatenating the positive integers:
#0.123456789101112131415161718192021...
#If dn represents the nth digit of the fractional part, find the value of the following expression.
#d1 × d10 × d100 × d1000 × d10000 × d100000 × d1000000

import functools as ft

def gen(n):
	digs = ''
	i = 1
	while len(digs) < n:
		digs += str(i)
		i += 1
	return int(digs[n-1])

print(ft.reduce((lambda a,b:a*b), [gen(10**i) for i in range(7)]))
