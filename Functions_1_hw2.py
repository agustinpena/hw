# Functions_1, HW 2
# Августин Пенья

def make_it_count(func):
	"""Make_it_Count Decorator"""
	globals()['counter_name'] = 0
	def wrapper(*args, **kwargs):
		global counter_name
		globals()['counter_name'] += 1
		return func(*args, **kwargs)
	return wrapper

