# Types and Data Structures, HW 5
# Августин Пенья

# Find the sum of all numbers, less than one million,
# which are palindromic in  base 10 and base 2.

def pal(c):
	"""Determines whether the string c is palindrome or not"""
	rc = ''
	for i in range(len(c)):
		rc += c[-i-1]
	if c == rc:
		return True
	else:
		return False

sum = 0
for c in range(10**6):
	if pal(str(c)) and pal(bin(c)[2:]):
		sum += c
print(sum)
