# Functional_Programming, HW 3
# Августин Пенья
# Calculate the number of steps for the number n, according to the Collatz hypothesis necessary to achieve 1.

def collatz_steps(x):
	op = lambda n: n/2 if n%2==0 else 3*n+1
	results = [op(x)]
	while results[-1] != 1:
		results.append(op(results[-1]))
	return len(results)
