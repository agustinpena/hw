# Functions_1 - HW3
# Августин Пенья 

def atom(x):
	global local
	local = x
	def get_value():
		global local
		return local

	def set_value(y):
		global local
		local = y
		return local

	def process_value(*args, **kwargs):
		global local
		for f in args:
			local = f(local)
		return local

	return get_value, set_value, process_value
