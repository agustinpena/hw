# Functional_programming, HW 1
# Августин Пенья

# -----First Method------
import functools as ft
squares = [x**2 for x in range(10**3)]
sum = ft.reduce((lambda a,b:a+b), squares)
print(sum)

#----Second Method------
import math
import functools as ft
nums = [x for x in range(10**6) if math.sqrt(x)==int(math.sqrt(x))]
sum = ft.reduce((lambda a,b:a+b), nums)
print(sum)

#-----Third Method------
x = 10**3 -1
s = x*(x+1)*(2*x + 1)*1/6
print(s)
