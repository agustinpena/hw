# Functions_2, HW 4
# Августин Пенья
# Rewrite the make_cache function, which saves The results of previous calls to the wrapped function,
# keeping results in storage for a time t, passed as a parameter to the decorator.

def make_cache(function, t):
	import time
	cache = {}
	def wrapper(*args):
		expired = []
		for x in cache.keys():
			if x+t < time.time():
				expired.append(x)
		for x in expired:
			del cache[x]
		start_time = time.time()
		val = function(*args)
		cache[start_time] = val
		return val

	return wrapper 
