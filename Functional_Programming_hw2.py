# Functional_Programming, HW2
#  Августин Пенья

def is_armstrong(x):
	import functools as ft
	n = len(str(x))
	digits = [int(d) for d in str(x)]
	powers = map((lambda y:y**n), digits)
	sum_powers = ft.reduce((lambda a,b:a+b), powers)
	if sum_powers == x:
		return True
	else:
		return False			
