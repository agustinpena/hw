# Functions_2 - HW1
# Августин Пенья

# Write a program that recieves text in a loop and determines recursively if
# it can be converted into a number. If so, divide by two for an even
# number, and multiply by 3 and add 1 for an odd number. If the text is 'cancel',
# the loop is terminated.

def sn(c):
	"""Checks recursively whether a string can be converted into a number or not"""
	n = len(c)
	if n == 1:
		if c.isdecimal():
			return True
		else:
			return False
	else:
		if sn(c[:n-1]) and sn(c[n-1]):
			return True
		else:
			return False


while True:
	cad = input('Enter text: -> ')
	if cad == 'cancel':
		print('Bye!')
		break
	elif sn(cad):
		cad = int(cad)
		if cad%2==0:
			print(cad/2)
		else:
			print(3*cad+1)	
	else:
		print('Could not convert the text into a number')

