# Functions_1 - HW1
# Августин Пенья

def partial(func, *fixated_args, **fixated_kwargs):
	def wrapper_partial(*args, **kwargs):
		return func(*fixated_args,*args, **fixated_kwargs, **kwargs)
	wrapper_partial.__doc__ = "A partial implementation of "+ func.__name__ + " with pre-applied arguments, being: " + str(fixated_args) + ' ' + str(fixated_kwargs)
	wrapper_partial.__name__ = "partial_"+func.__name__
	return wrapper_partial
