# Functions_2 - HW2
# Августин Пенья

#def mysqrt(m, acc = 0):
#    import math
#    x = 1
#    for i in range(10**5):
#        x = (x ** 2 + m) / (2 * x)
#    if acc != 0:
#        x = round(x, -int(math.log10(acc)))
#    return x



def raiz(m, acc = 0):
#       import math
#       contador = 0
        ant = 0
        n = (1+m)/2
        while abs(1-ant/n) > 1e-14:
                ant = n
                n = (n**2+m)/(2*n)
#               print('Iteración '+str(contador)+': ' + str(n))
#               print('Error en it. '+str(contador)+': '+str(abs(n-ant)))
#               contador += 1

#       print('Núm de iteraciones: '+str(contador))
#       print('Valor exa: '+str(math.sqrt(m)))
#       print('Valor cal:' +str(n))
        
        if acc != 0:
                return (int(n/acc))*acc
        else:
                return n

