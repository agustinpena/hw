# Functions_2 - HW 5
# Августин Пенья

# Write a decorator counting a function's total number of calls
# and total execution time. Using this function check the 
# execution time for various Fibonacci numbers algorithms.
# Find the most optimal.

def profile(func):
	import time
	global storage
	global i
	storage = i = 0

	def wrap(*args, **kwargs):
		global storage
		global i
		i += 1
		start_time = time.time()
		result = func(*args, **kwargs)
		total_time = time.time() - start_time
		storage += total_time
		print('Num of calls: ' + str(i) + ' | Global Execution Time: ' + str(storage))
		return result

	return wrap

# The profile function was used to time the following methods
# to find Fibonacci numbers, for the 30-th Fibonacci number.
# The fastest is the LOOP method:

# Method: LOOP--------------------------------
def fibl(n):
        """Finds the n-th Fib num with a LOOP """
        a,b = 0,1
        for i in range(n):
                a,b = b,a+b
        return a

# Num of calls: 1 | Global Execution Time: 1.9073486328125e-05
# fibl(30) = 832040


# Method: RECURSION --------------------------
def fibr(n):
        """Finds the n-th Fib num with RECURSION"""
        if n==1 or n==2:
                return 1
        return fibr(n-1)+fibr(n-2)
# Num of calls: 1 | Global Execution Time: 0.5611860752105713
# fibr(30) = 832040


# Method: GENERATORS ------------------------
def fibg(n):
        """Finds the n-th Fib num with GENERATORS"""
        def gen():
                a,b = 0,1
                while True:
                        a,b = b,a+b
                        yield a
        z = gen()
        if n == 0: 
                return 0
        elif n == 1:
                return 1
        else:
                for i in range(n+1):
                        next(z) 
                        if i == n-2:
                                return next(z)
# Num of calls: 1 | Global Execution Time: 3.314018249511719e-05
# fibg(30) = 832040


# Method: Memoization ----------------------
def fibm(n):
        """Finds the n-th Fib num with MEMOIZATION"""

        def memoize(func, arg):
                memo = {}
                if arg not in memo:
                        memo[arg] = func(arg)
                        return memo[arg]

        def fibl(n):
                """Finds the n-th Fib num with a LOOP """
                a,b = 0,1
                for i in range(n):
                        a,b = b,a+b
                return a

        return memoize(fibl,n)

# Num of calls: 1 | Global Execution Time: 2.384185791015625e-05
# 832040

